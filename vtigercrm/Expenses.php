<?php
include_once 'vtlib/Vtiger/Module.php';
include_once('support/Support.php');

$Vtiger_Utils_Log = true;

$MODULENAME = 'Expenses';
$PARENTNAME = 'SUPPORT';
$INITIAL_VERSION = '1.0';

$support = new Support();

$moduleInstance = Vtiger_Module::getInstance($MODULENAME);
if ($moduleInstance) {
        echo 'Module '.$MODULENAME.' already present with version '.$moduleInstance->version;
        
        //gestisco le versioni
        
        //1.0 => 1.1
        if ($moduleInstance->version == "1.0"){
            //faccio la versione 1.1
            $moduleInstance->version = '1.1';
            $moduleInstance->save();
            
            $block1_1 = new Vtiger_Block();
            $block1_1->label = 'LBL_1_1';
            $moduleInstance->addBlock($block1_1);
            
            $field1_1  = new Vtiger_Field();
            $field1_1->name = 'newField';
            $field1_1->label= 'New Field';
            $field1_1->uitype= 2;
            $field1_1->column = $field1->name;
            $field1_1->columntype = 'VARCHAR(255)';
            $field1_1->typeofdata = 'V~O';
            $block1_1->addField($field1_1);

            $moduleInstance->__updateVersion('1.1');
        }
        
        //1.1 => 1.2
        if ($moduleInstance->version == "1.1"){
            //inserisce nuova funzionalita
            //TODO
            
            $moduleInstance->__updateVersion('1.2');
        }
            
} 
else {
        //prima installazione del modulo
        $moduleInstance = new Vtiger_Module();
        $moduleInstance->name = $MODULENAME;
        $moduleInstance->parent = $PARENTNAME;
        $moduleInstance->version = $INITIAL_VERSION;
        $moduleInstance->save();
               
        // Schema Setup
        $moduleInstance->initTables();

        // Field Setup
        $block = new Vtiger_Block();
        $block->label = 'LBL_'. strtoupper($moduleInstance->name) . '_INFORMATION';
        $moduleInstance->addBlock($block);

        $blockcf = new Vtiger_Block();
        $blockcf->label = 'LBL_CUSTOM_INFORMATION';
        $moduleInstance->addBlock($blockcf);

        $field1  = new Vtiger_Field();
        $field1->name = 'summary';
        $field1->label= 'Summary';
        $field1->uitype= 2;
        $field1->column = $field1->name;
        $field1->columntype = 'VARCHAR(255)';
        $field1->typeofdata = 'V~O';
        $block->addField($field1);

        $moduleInstance->setEntityIdentifier($field1);

        $field2  = new Vtiger_Field();
        $field2->name = 'expenseon';
        $field2->label= 'Expense On';
        $field2->uitype= 5;
        $field2->column = $field2->name;
        $field2->columntype = 'Date';
        $field2->typeofdata = 'D~O';
        $block->addField($field2);

        $field3  = new Vtiger_Field();
        $field3->name = 'expenseamount';
        $field3->label= 'Amount';
        $field3->uitype= 71;
        $field3->column = $field3->name;
        $field3->columntype = 'VARCHAR(255)';
        $field3->typeofdata = 'V~O';
        $block->addField($field3);

        $field3  = new Vtiger_Field();
        $field3->name = 'description';
        $field3->label= 'Description';
        $field3->uitype= 19;
        $field3->column = 'description';
        $field3->table = 'vtiger_crmentity';
        $blockcf->addField($field3);

        // Recommended common fields every Entity module should have (linked to core table)
        $mfield1 = new Vtiger_Field();
        $mfield1->name = 'assigned_user_id';
        $mfield1->label = 'Assigned To';
        $mfield1->table = 'vtiger_crmentity';
        $mfield1->column = 'assigned_user_id';
        $mfield1->uitype = 53;
        $mfield1->typeofdata = 'V~O';
        $block->addField($mfield1);

        $mfield2 = new Vtiger_Field();
        $mfield2->name = 'CreatedTime';
        $mfield2->label= 'Created Time';
        $mfield2->table = 'vtiger_crmentity';
        $mfield2->column = 'createdtime';
        $mfield2->uitype = 70;
        $mfield2->typeofdata = 'T~O';
        $mfield2->displaytype= 2;
        $block->addField($mfield2);

        $mfield3 = new Vtiger_Field();
        $mfield3->name = 'ModifiedTime';
        $mfield3->label= 'Modified Time';
        $mfield3->table = 'vtiger_crmentity';
        $mfield3->column = 'modifiedtime';
        $mfield3->uitype = 70;
        $mfield3->typeofdata = 'T~O';
        $mfield3->displaytype= 2;
        $block->addField($mfield3);

        // Filter Setup
        $filter1 = new Vtiger_Filter();
        $filter1->name = 'All';
        $filter1->isdefault = true;
        $moduleInstance->addFilter($filter1);
        $filter1->addField($field1)->addField($field2, 1)->addField($field3, 2)->addField($mfield1, 3);

        // Sharing Access Setup
        $moduleInstance->setDefaultSharing();

        // Webservice Setup
        $moduleInstance->initWebservice();
        
        Settings_MenuEditor_Module_Model::addModuleToApp($MODULENAME, $PARENTNAME);

        echo 'Module '.$MODULENAME. created;   
}