<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
include_once('vtlib/Vtiger/Access.php');
include_once('vtlib/Vtiger/Block.php');
include_once('vtlib/Vtiger/Field.php');
include_once('vtlib/Vtiger/Filter.php');
include_once('vtlib/Vtiger/Profile.php');
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Link.php');
include_once('vtlib/Vtiger/Event.php');
include_once('vtlib/Vtiger/Webservice.php');
include_once('vtlib/Vtiger/Version.php');
require_once 'includes/runtime/Cache.php';

/**
 * Classe di Support
 */
class Support {

	/**
	 * Constructor
	 */
	function __construct() {
	}
	
	public function insertVtigerAppToTab($MODULENAME, $PARENTNAME) {
	    
	    self::log("insert VtigerAppToTab of module $MODULENAME. ... STARTED");
	    
	    //recupero tabid (PK) della tabella vtiger_tab
	    global $adb;
	    $result = $adb->pquery("SELECT tabid AS tabid FROM vtiger_tab where name=?", Array($MODULENAME));
	    //se tabid esiste entro nell'if
	    if ($adb->num_rows($result) == 1){
	        $tabid = $adb->query_result($result, 0, 'tabid');
	        //recupero max sequence (se esiste, se non esiste la setto a 0) del parent name che ci interessa
	        $seqResult = $adb->pquery('SELECT max(sequence) as sequence FROM vtiger_app2tab WHERE appname=?', array($PARENTNAME));
	        $sequence = $adb->query_result($seqResult, 0, 'sequence');
	        if (!isset($sequence)) {
	            self::log("Sequence for parent $PARENTNAME not present. Init sequence");
	            $sequence = 0;
	        }
	        //verifico che non esista nessuna tupla in vtiger_app2tab con la stessa coppia tabid - appname
	        $tuplaGiaPresente = $adb->pquery('SELECT tabid FROM vtiger_app2tab WHERE appname=? AND tabid=?', array($PARENTNAME, $tabid));
	        $existingTabId = $adb->query_result($tuplaGiaPresente, 0, 'tabid');
	        if (!$existingTabId) {
	            //faccio insert
	            $adb->pquery('INSERT INTO vtiger_app2tab(tabid,appname,sequence,visible) values(?,?,?,?)', array($tabid, $PARENTNAME, $sequence+11, 1));
	        }
	        else{
	            self::log("Row in vtiger_app2tab with tabid $tabid and appname $PARENTNAME is already present!!!!");
	        }           	        
	    }
	    else{	        
	        self::log("$MODULENAME is not installed (not present in vtiger_tab)");
	    }
	    
	    self::log("insert VtigerAppToTab of module $MODULENAME. ... FINISH");	    	    
	}
        
        public function deleteVtigerAppToTab($tabid) {
	    
	    self::log("delete VtigerAppToTab of module with tabid $tabid. ... STARTED");
            
            global $adb;
	    $adb->pquery("delete FROM vtiger_app2tab where tabid=?", Array($tabid));
            
            self::log("delete VtigerAppToTab of module with tabid $tabid. ... FINISH");            
        }
        
	
	static function log($message, $delimit=true) {
	    Vtiger_Utils::Log($message, $delimit);
	}


}
?>
