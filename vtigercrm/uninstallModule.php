<?php

include_once 'vtlib/Vtiger/Module.php';
include_once('support/Support.php');

$Vtiger_Utils_Log = true;

$support = new Support();

$moduleName = 'Expenses';
        
$module = Vtiger_Module::getInstance($moduleName);
if ($module){ 
    $support->deleteVtigerAppToTab($module->getId);
    $module->delete();

}
else {
    echo $moduleName.' not present';
}
